#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX 2000

int randint(int also, int felso)
{
    int veletlen = rand();
    int intervallum = felso - also + 1;

    veletlen = veletlen % intervallum;
    veletlen = veletlen + also;

    return veletlen;
}

void file_error_exit(char *fname)
{
    fprintf(stderr, "Error: '%s' cannot be opened!\n", fname);
    exit(1);
}

int main()
{
    srand(time(NULL));
    
    char* fname = "numbers.txt";
    FILE *out = fopen(fname, "w");
    if(out == NULL)
    {
        file_error_exit(fname);
    }

    for(int i = 1; i <= 100; i++)
    {
        fprintf(out, "%d\n", randint(10, 99));
    }

    fclose(out);

    return 0;
}
